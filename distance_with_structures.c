#include<stdio.h>
#include<math.h>
struct points
{
    float x, y;
};
 
float distance(struct points m, struct points n)
{
    float result;
    result = sqrt((m.x - n.x) * (m.x - n.x) + (m.y - n.y) *(m.y - n.y));
    return result;
}
 
int main()
{
    struct points m, n;
    printf("Enter the coordinates of point A\n");
    printf("X - axis coordinate:\n");
    scanf("%f", &m.x);
    printf("Y - axis Coordinate:\n");
    scanf("%f", &m.y);  
    printf("Enter the coordinates of point B\n");
    printf("Y - axis Coordinate:\n");
    scanf("%f", &n.x);
    printf("Y - axis coordinate:\n");
    scanf("%f", &n.y);
    printf("Distance between points A and B: \n%f", distance(m, n));
    return 0;
}
//WAP to find the distance between two points using structures and 4 functions.