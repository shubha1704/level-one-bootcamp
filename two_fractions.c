#include<stdio.h>
struct fraction
{
  int n;
  int d;

};
typedef struct fraction Fraction;

Fraction sum (Fraction f1, Fraction f2)
{
  Fraction retval;
  int x, y,gcd;
  x = (f1.n * f2.d) + (f2.n * f1.d);
  y = (f1.d * f2.d);
 for(int i = 1; i<=x && i<=y;i++)
 {
     if(x%i==0 && y%i==0)
      gcd=i;
 }

  retval.n = x / gcd;
  retval.d = y / gcd;
  return retval;
}
Fraction input (int a)
{
  Fraction retval;
  printf ("Enter fraction %d: numerator and denominator:", a);
  scanf ("%d%d", & retval.n, &retval.d);
  return retval;
}

void display (Fraction f1, Fraction f2, Fraction r)
{
  printf ("Result of %d/%d + %d/%d = %d/%d", f1.n, f1.d, f2.n, f2.d,r.n, r.d);
}

int main ()
{
  Fraction f1 = input (1);
  Fraction f2 = input (2);
  Fraction r = sum (f1, f2);
  display (f1, f2, r);
  return 0;
}

//WAP to find the sum of two fractions.